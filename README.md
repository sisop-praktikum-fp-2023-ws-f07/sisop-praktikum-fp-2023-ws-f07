# sisop-praktikum-fp-2023-ws-f07

| Nama | NRP |
| ---------------------- | ---------- |
|M Arkan Karindra Darvesh | 5025211236 |
| Meyroja Jovancha Firoos | 5025211204 |
| Dany Dary | 5025211237 |



## Penjelasan  

# code
```
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>
#include <limits.h>

#define PORT 8631
```
Code diatas berfungsi untuk mengimport Library serta Konstanta.
```
struct permission
{
  char name[10000];
  char pass[10000];
};
```
Code diatas berfungsi sebagai struktur data.
```
int checkPerms(char *username, char *password)
{
  FILE *file;
  struct permission client;
  int flag = 0;
  file = fopen("../database/databases/user.dat", "rb");

  while (1)
  {
    fread(&client, sizeof(client), 1, file);
    if (strcmp(client.name, username) == 0)
    {
      if (strcmp(client.pass, password) == 0)
        flag = 1;
    }
    if (feof(file))
      break;
  }

  fclose(file);

  if (flag == 0)
  {
    printf("Not permitted\n");
    exit(1);
  }
  else
    return 1;
}
```
Code diatas merupakan Fungsi checkPerms. Fungsi ini digunakan untuk memeriksa izin akses pengguna dengan membandingkan nama pengguna (username) dan kata sandi (password) yang diberikan dengan entri yang ada dalam file user.dat .
```
void writeLog(char *cmd, char *name)
{
  time_t times;
  struct tm *timestamp;
  time(&times);
  timestamp = localtime(&times);

  char log_entry[1000];

  FILE *file;
  char loc[10000];
  snprintf(loc, sizeof loc, "../database/log/log%s.log", name);
  file = fopen(loc, "ab");

  sprintf(log_entry, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n", timestamp->tm_year + 1900, timestamp->tm_mon + 1, timestamp->tm_mday, timestamp->tm_hour, timestamp->tm_min, timestamp->tm_sec, name, cmd);

  fputs(log_entry, file);
  fclose(file);
}
```
Code diatas merupakan Fungsi writeLog. Fungsi write Log memiliki fungsi  untuk menulis entri log berdasarkan perintah (cmd) yang diberikan oleh pengguna dan nama pengguna (name) ke file log yang sesuai.
```
int main(int argc, char *argv[])
{
  int userID = geteuid();

  int client_socket, ret;
  struct sockaddr_in serverAddr;
  char buffer[32000];

  client_socket = socket(AF_INET, SOCK_STREAM, 0);

  // ...

  while (1)
  {
    // ...

    if (userID != 0)
    {
      // ...
    }

    printf("Client: \t");
    scanf(" %[^\n]s", input);
    strcpy(temp, input);
    token = strtok(input, " ");

    while (token != NULL)
    {
      strcpy(cmd[i], token);
      i++;
      token = strtok(NULL, " ");
    }

    int falseCommand = 0;
    if (strcmp(cmd[0], "CREATE") == 0)
    {
      // ...
    }
    else if (strcmp(cmd[0], "GRANT") == 0 && strcmp(cmd[1], "PERMISSION") == 0 && strcmp(cmd[3], "INTO") == 0)
    {
      // ...
    }
    else if (strcmp(cmd[0], "USE") == 0)
    {
      // ...
    }
    else if (strcmp(cmd[0], "checkDatabase") == 0)
    {
      // ...
    }
    else if (strcmp(cmd[0], "DROP") == 0)
    {
      // ...
    }
    else if (strcmp(cmd[0], "INSERT") == 0 && strcmp(cmd[1], "INTO") == 0)
    {
      // ...
    }
    else if (strcmp(cmd[0], "UPDATE") == 0)
    {
      // ...
    }
    else if (strcmp(cmd[0], "DELETE") == 0)
    {
      // ...
    }
    else if (strcmp(cmd[0], "SELECT") == 0)
    {
      // ...
    }
    else if (strcmp(cmd[0], ":exit") != 0)
    {
      falseCommand = 1;
      char peringatan[] = "Invalid Command";
      send(client_socket, peringatan, strlen(peringatan), 0);
    }

    if (falseCommand != 1)
    {
      char sender[10000];
      if (userID == 0)
        strcpy(sender, "root");
      else
        strcpy(sender, argv[2]);
      writeLog(temp, sender);
    }

    if (strcmp(cmd[0], ":exit") == 0)
    {
      send(client_socket, cmd[0], strlen(cmd[0]), 0);
      close(client_socket);
      printf("[-] Disconnected\n");
      exit(1);
    }
    bzero(buffer, sizeof(buffer));
    if (recv(client_socket, buffer, 1024, 0) < 0)
      printf("[-]Error in receiving data.\n");
    else
      printf("Server: \t%s\n", buffer);
  }

  return 0;
}
```
Lalu yang terakhir ada Fungsi main.Fungsi main berisi logika utama program. Ini meliputi penanganan socket, interaksi dengan pengguna untuk memasukkan perintah, memecah perintah, memeriksa dan mengirim perintah ke server, serta menangani log dan respon dari server.


