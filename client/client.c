#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>
#include <limits.h>

#define PORT 8631

struct permission
{
  char name[10000];
  char pass[10000];
};

int checkPerms(char *username, char *password)
{
  FILE *file;
  struct permission client;
  int flag = 0;
  file = fopen("../database/databases/user.dat", "rb");

  while (1)
  {
    fread(&client, sizeof(client), 1, file);
    if (strcmp(client.name, username) == 0)
    {
      if (strcmp(client.pass, password) == 0)
        flag = 1;
    }
    if (feof(file))
      break;
  }

  fclose(file);

  if (flag == 0)
  {
    printf("Not permitted\n");
    exit(1);
  }
  else
    return 1;
}

void writeLog(char *cmd, char *name)
{
  time_t times;
  struct tm *timestamp;
  time(&times);
  timestamp = localtime(&times);

  char log_entry[1000];

  FILE *file;
  char loc[10000];
  snprintf(loc, sizeof loc, "../database/log/log%s.log", name);
  file = fopen(loc, "ab");

  sprintf(log_entry, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n", timestamp->tm_year + 1900, timestamp->tm_mon + 1, timestamp->tm_mday, timestamp->tm_hour, timestamp->tm_min, timestamp->tm_sec, name, cmd);

  fputs(log_entry, file);
  fclose(file);
}

int main(int argc, char *argv[])
{
  int userID = geteuid();

  int client_socket, ret;
  struct sockaddr_in serverAddr;
  char buffer[32000];

  client_socket = socket(AF_INET, SOCK_STREAM, 0);

  if (client_socket < 0)
  {
    printf("[-]Error in connection.\n");
    exit(1);
  }

  printf("[+]Client Socket is created.\n");

  memset(&serverAddr, '\0', sizeof(serverAddr));
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(PORT);
  serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

  ret = connect(client_socket, (struct sockaddr *)&serverAddr, sizeof(serverAddr));

  if (ret < 0)
  {
    printf("[-]Error connection.\n");
    exit(1);
  }
  printf("[+]Connected to Server.\n");

  while (1)
  {
    char input[10000];
    char temp[10000];
    char cmd[100][10000];
    char *token;
    int i = 0;

    if (userID != 0)
    {
      char validateLogin[10000], login[1024];
      snprintf(validateLogin, sizeof validateLogin, "checkUser-%s-%s-%d", argv[2], argv[4], userID);
      send(client_socket, validateLogin, strlen(validateLogin), 0);
      strcpy(validateLogin, "");

      int readMsg;
      readMsg = read(client_socket, login, 1024);
      if (strcmp(login, "Cannot Login") == 0)
      {
        close(client_socket);
        printf("[-] Invalid Credentials\n");
        exit(1);
      }
    }

    printf("Client: \t");
    scanf(" %[^\n]s", input);
    strcpy(temp, input);
    token = strtok(input, " ");

    while (token != NULL)
    {
      strcpy(cmd[i], token);
      i++;
      token = strtok(NULL, " ");
    }

    int falseCommand = 0;
    if (strcmp(cmd[0], "CREATE") == 0)
    {
      if (strcmp(cmd[1], "USER") == 0 && strcmp(cmd[3], "IDENTIFIED") == 0 && strcmp(cmd[4], "BY") == 0)
      {
        snprintf(buffer, sizeof buffer, "createUser-%s-%s-%d", cmd[2], cmd[5], userID);
        send(client_socket, buffer, strlen(buffer), 0);
      }
      else if (strcmp(cmd[1], "DATABASE") == 0)
      {
        snprintf(buffer, sizeof buffer, "createDatabase-%s-%s-%d", cmd[2], argv[2], userID);
        send(client_socket, buffer, strlen(buffer), 0);
      }
      else if (strcmp(cmd[1], "TABLE") == 0)
      {
        snprintf(buffer, sizeof buffer, "createTable-%s", temp);
        send(client_socket, buffer, strlen(buffer), 0);
      }
    }
    else if (strcmp(cmd[0], "GRANT") == 0 && strcmp(cmd[1], "PERMISSION") == 0 && strcmp(cmd[3], "INTO") == 0)
    {
      snprintf(buffer, sizeof buffer, "grantPermission-%s-%s-%d", cmd[2], cmd[4], userID);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    else if (strcmp(cmd[0], "USE") == 0)
    {
      snprintf(buffer, sizeof buffer, "useDatabase-%s-%s-%d", cmd[1], argv[2], userID);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    else if (strcmp(cmd[0], "checkDatabase") == 0)
    {
      snprintf(buffer, sizeof buffer, "%s", cmd[0]);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    else if (strcmp(cmd[0], "DROP") == 0)
    {
      if (strcmp(cmd[1], "DATABASE") == 0)
      {
        snprintf(buffer, sizeof buffer, "dropDatabase-%s-%s", cmd[2], argv[2]);
        send(client_socket, buffer, strlen(buffer), 0);
      }
      else if (strcmp(cmd[1], "TABLE") == 0)
      {
        snprintf(buffer, sizeof buffer, "dropTable-%s-%s", cmd[2], argv[2]);
        send(client_socket, buffer, strlen(buffer), 0);
      }
      else if (strcmp(cmd[1], "COLUMN") == 0)
      {
        snprintf(buffer, sizeof buffer, "dropColumn-%s-%s-%s", cmd[2], cmd[4], argv[2]);
        send(client_socket, buffer, strlen(buffer), 0);
      }
    }
    else if (strcmp(cmd[0], "INSERT") == 0 && strcmp(cmd[1], "INTO") == 0)
    {
      snprintf(buffer, sizeof buffer, "insert-%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    else if (strcmp(cmd[0], "UPDATE") == 0)
    {
      snprintf(buffer, sizeof buffer, "update-%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    else if (strcmp(cmd[0], "DELETE") == 0)
    {
      snprintf(buffer, sizeof buffer, "delete-%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    else if (strcmp(cmd[0], "SELECT") == 0)
    {
      snprintf(buffer, sizeof buffer, "select-%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    else if (strcmp(cmd[0], ":exit") != 0)
    {
      falseCommand = 1;
      char peringatan[] = "Invalid Command";
      send(client_socket, peringatan, strlen(peringatan), 0);
    }

    if (falseCommand != 1)
    {
      char sender[10000];
      if (userID == 0)
        strcpy(sender, "root");
      else
        strcpy(sender, argv[2]);
      writeLog(temp, sender);
    }

    if (strcmp(cmd[0], ":exit") == 0)
    {
      send(client_socket, cmd[0], strlen(cmd[0]), 0);
      close(client_socket);
      printf("[-] Disconnected\n");
      exit(1);
    }
    bzero(buffer, sizeof(buffer));
    if (recv(client_socket, buffer, 1024, 0) < 0)
      printf("[-]Error in receiving data.\n");
    else
      printf("Server: \t%s\n", buffer);
  }

  return 0;
}
